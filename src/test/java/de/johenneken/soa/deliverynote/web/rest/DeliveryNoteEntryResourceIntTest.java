package de.johenneken.soa.deliverynote.web.rest;

import de.johenneken.soa.deliverynote.DeliveryNoteServiceApp;

import de.johenneken.soa.deliverynote.domain.DeliveryNoteEntry;
import de.johenneken.soa.deliverynote.repository.DeliveryNoteEntryRepository;
import de.johenneken.soa.deliverynote.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static de.johenneken.soa.deliverynote.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DeliveryNoteEntryResource REST controller.
 *
 * @see DeliveryNoteEntryResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DeliveryNoteServiceApp.class)
public class DeliveryNoteEntryResourceIntTest {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_AMOUNT = 1;
    private static final Integer UPDATED_AMOUNT = 2;

    @Autowired
    private DeliveryNoteEntryRepository deliveryNoteEntryRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDeliveryNoteEntryMockMvc;

    private DeliveryNoteEntry deliveryNoteEntry;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DeliveryNoteEntryResource deliveryNoteEntryResource = new DeliveryNoteEntryResource(deliveryNoteEntryRepository);
        this.restDeliveryNoteEntryMockMvc = MockMvcBuilders.standaloneSetup(deliveryNoteEntryResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryNoteEntry createEntity(EntityManager em) {
        DeliveryNoteEntry deliveryNoteEntry = new DeliveryNoteEntry()
            .description(DEFAULT_DESCRIPTION)
            .amount(DEFAULT_AMOUNT);
        return deliveryNoteEntry;
    }

    @Before
    public void initTest() {
        deliveryNoteEntry = createEntity(em);
    }

    @Test
    @Transactional
    public void createDeliveryNoteEntry() throws Exception {
        int databaseSizeBeforeCreate = deliveryNoteEntryRepository.findAll().size();

        // Create the DeliveryNoteEntry
        restDeliveryNoteEntryMockMvc.perform(post("/api/delivery-note-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNoteEntry)))
            .andExpect(status().isCreated());

        // Validate the DeliveryNoteEntry in the database
        List<DeliveryNoteEntry> deliveryNoteEntryList = deliveryNoteEntryRepository.findAll();
        assertThat(deliveryNoteEntryList).hasSize(databaseSizeBeforeCreate + 1);
        DeliveryNoteEntry testDeliveryNoteEntry = deliveryNoteEntryList.get(deliveryNoteEntryList.size() - 1);
        assertThat(testDeliveryNoteEntry.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testDeliveryNoteEntry.getAmount()).isEqualTo(DEFAULT_AMOUNT);
    }

    @Test
    @Transactional
    public void createDeliveryNoteEntryWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = deliveryNoteEntryRepository.findAll().size();

        // Create the DeliveryNoteEntry with an existing ID
        deliveryNoteEntry.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeliveryNoteEntryMockMvc.perform(post("/api/delivery-note-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNoteEntry)))
            .andExpect(status().isBadRequest());

        // Validate the DeliveryNoteEntry in the database
        List<DeliveryNoteEntry> deliveryNoteEntryList = deliveryNoteEntryRepository.findAll();
        assertThat(deliveryNoteEntryList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllDeliveryNoteEntries() throws Exception {
        // Initialize the database
        deliveryNoteEntryRepository.saveAndFlush(deliveryNoteEntry);

        // Get all the deliveryNoteEntryList
        restDeliveryNoteEntryMockMvc.perform(get("/api/delivery-note-entries?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deliveryNoteEntry.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].amount").value(hasItem(DEFAULT_AMOUNT)));
    }
    
    @Test
    @Transactional
    public void getDeliveryNoteEntry() throws Exception {
        // Initialize the database
        deliveryNoteEntryRepository.saveAndFlush(deliveryNoteEntry);

        // Get the deliveryNoteEntry
        restDeliveryNoteEntryMockMvc.perform(get("/api/delivery-note-entries/{id}", deliveryNoteEntry.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(deliveryNoteEntry.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.amount").value(DEFAULT_AMOUNT));
    }

    @Test
    @Transactional
    public void getNonExistingDeliveryNoteEntry() throws Exception {
        // Get the deliveryNoteEntry
        restDeliveryNoteEntryMockMvc.perform(get("/api/delivery-note-entries/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDeliveryNoteEntry() throws Exception {
        // Initialize the database
        deliveryNoteEntryRepository.saveAndFlush(deliveryNoteEntry);

        int databaseSizeBeforeUpdate = deliveryNoteEntryRepository.findAll().size();

        // Update the deliveryNoteEntry
        DeliveryNoteEntry updatedDeliveryNoteEntry = deliveryNoteEntryRepository.findById(deliveryNoteEntry.getId()).get();
        // Disconnect from session so that the updates on updatedDeliveryNoteEntry are not directly saved in db
        em.detach(updatedDeliveryNoteEntry);
        updatedDeliveryNoteEntry
            .description(UPDATED_DESCRIPTION)
            .amount(UPDATED_AMOUNT);

        restDeliveryNoteEntryMockMvc.perform(put("/api/delivery-note-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDeliveryNoteEntry)))
            .andExpect(status().isOk());

        // Validate the DeliveryNoteEntry in the database
        List<DeliveryNoteEntry> deliveryNoteEntryList = deliveryNoteEntryRepository.findAll();
        assertThat(deliveryNoteEntryList).hasSize(databaseSizeBeforeUpdate);
        DeliveryNoteEntry testDeliveryNoteEntry = deliveryNoteEntryList.get(deliveryNoteEntryList.size() - 1);
        assertThat(testDeliveryNoteEntry.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testDeliveryNoteEntry.getAmount()).isEqualTo(UPDATED_AMOUNT);
    }

    @Test
    @Transactional
    public void updateNonExistingDeliveryNoteEntry() throws Exception {
        int databaseSizeBeforeUpdate = deliveryNoteEntryRepository.findAll().size();

        // Create the DeliveryNoteEntry

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeliveryNoteEntryMockMvc.perform(put("/api/delivery-note-entries")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNoteEntry)))
            .andExpect(status().isBadRequest());

        // Validate the DeliveryNoteEntry in the database
        List<DeliveryNoteEntry> deliveryNoteEntryList = deliveryNoteEntryRepository.findAll();
        assertThat(deliveryNoteEntryList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDeliveryNoteEntry() throws Exception {
        // Initialize the database
        deliveryNoteEntryRepository.saveAndFlush(deliveryNoteEntry);

        int databaseSizeBeforeDelete = deliveryNoteEntryRepository.findAll().size();

        // Get the deliveryNoteEntry
        restDeliveryNoteEntryMockMvc.perform(delete("/api/delivery-note-entries/{id}", deliveryNoteEntry.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DeliveryNoteEntry> deliveryNoteEntryList = deliveryNoteEntryRepository.findAll();
        assertThat(deliveryNoteEntryList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeliveryNoteEntry.class);
        DeliveryNoteEntry deliveryNoteEntry1 = new DeliveryNoteEntry();
        deliveryNoteEntry1.setId(1L);
        DeliveryNoteEntry deliveryNoteEntry2 = new DeliveryNoteEntry();
        deliveryNoteEntry2.setId(deliveryNoteEntry1.getId());
        assertThat(deliveryNoteEntry1).isEqualTo(deliveryNoteEntry2);
        deliveryNoteEntry2.setId(2L);
        assertThat(deliveryNoteEntry1).isNotEqualTo(deliveryNoteEntry2);
        deliveryNoteEntry1.setId(null);
        assertThat(deliveryNoteEntry1).isNotEqualTo(deliveryNoteEntry2);
    }
}
