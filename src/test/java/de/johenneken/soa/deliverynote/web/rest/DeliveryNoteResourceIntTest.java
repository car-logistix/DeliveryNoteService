package de.johenneken.soa.deliverynote.web.rest;

import de.johenneken.soa.deliverynote.DeliveryNoteServiceApp;

import de.johenneken.soa.deliverynote.domain.DeliveryNote;
import de.johenneken.soa.deliverynote.repository.DeliveryNoteRepository;
import de.johenneken.soa.deliverynote.service.DeliveryNoteService;
import de.johenneken.soa.deliverynote.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static de.johenneken.soa.deliverynote.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the DeliveryNoteResource REST controller.
 *
 * @see DeliveryNoteResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = DeliveryNoteServiceApp.class)
public class DeliveryNoteResourceIntTest {

    private static final String DEFAULT_NOTE_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_NOTE_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_DELIVERY_ADRESS = "AAAAAAAAAA";
    private static final String UPDATED_DELIVERY_ADRESS = "BBBBBBBBBB";

    private static final String DEFAULT_SUPPLIER_FIRM = "AAAAAAAAAA";
    private static final String UPDATED_SUPPLIER_FIRM = "BBBBBBBBBB";

    private static final String DEFAULT_SUPPLIER_ADRESS = "AAAAAAAAAA";
    private static final String UPDATED_SUPPLIER_ADRESS = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_ISSUE_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_ISSUE_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_FINISHED = false;
    private static final Boolean UPDATED_FINISHED = true;

    private static final String DEFAULT_PRINCIPAL = "AAAAAAAAAA";
    private static final String UPDATED_PRINCIPAL = "BBBBBBBBBB";

    @Autowired
    private DeliveryNoteRepository deliveryNoteRepository;
    
    @Autowired
    private DeliveryNoteService deliveryNoteService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDeliveryNoteMockMvc;

    private DeliveryNote deliveryNote;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DeliveryNoteResource deliveryNoteResource = new DeliveryNoteResource(deliveryNoteService);
        this.restDeliveryNoteMockMvc = MockMvcBuilders.standaloneSetup(deliveryNoteResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DeliveryNote createEntity(EntityManager em) {
        DeliveryNote deliveryNote = new DeliveryNote()
            .noteNumber(DEFAULT_NOTE_NUMBER)
            .deliveryAdress(DEFAULT_DELIVERY_ADRESS)
            .supplierFirm(DEFAULT_SUPPLIER_FIRM)
            .supplierAdress(DEFAULT_SUPPLIER_ADRESS)
            .issueDate(DEFAULT_ISSUE_DATE)
            .finished(DEFAULT_FINISHED)
            .principal(DEFAULT_PRINCIPAL);
        return deliveryNote;
    }

    @Before
    public void initTest() {
        deliveryNote = createEntity(em);
    }

    @Test
    @Transactional
    public void createDeliveryNote() throws Exception {
        int databaseSizeBeforeCreate = deliveryNoteRepository.findAll().size();

        // Create the DeliveryNote
        restDeliveryNoteMockMvc.perform(post("/api/delivery-notes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNote)))
            .andExpect(status().isCreated());

        // Validate the DeliveryNote in the database
        List<DeliveryNote> deliveryNoteList = deliveryNoteRepository.findAll();
        assertThat(deliveryNoteList).hasSize(databaseSizeBeforeCreate + 1);
        DeliveryNote testDeliveryNote = deliveryNoteList.get(deliveryNoteList.size() - 1);
        assertThat(testDeliveryNote.getNoteNumber()).isEqualTo(DEFAULT_NOTE_NUMBER);
        assertThat(testDeliveryNote.getDeliveryAdress()).isEqualTo(DEFAULT_DELIVERY_ADRESS);
        assertThat(testDeliveryNote.getSupplierFirm()).isEqualTo(DEFAULT_SUPPLIER_FIRM);
        assertThat(testDeliveryNote.getSupplierAdress()).isEqualTo(DEFAULT_SUPPLIER_ADRESS);
        assertThat(testDeliveryNote.getIssueDate()).isEqualTo(DEFAULT_ISSUE_DATE);
        assertThat(testDeliveryNote.isFinished()).isEqualTo(DEFAULT_FINISHED);
        assertThat(testDeliveryNote.getPrincipal()).isEqualTo(DEFAULT_PRINCIPAL);
    }

    @Test
    @Transactional
    public void createDeliveryNoteWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = deliveryNoteRepository.findAll().size();

        // Create the DeliveryNote with an existing ID
        deliveryNote.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDeliveryNoteMockMvc.perform(post("/api/delivery-notes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNote)))
            .andExpect(status().isBadRequest());

        // Validate the DeliveryNote in the database
        List<DeliveryNote> deliveryNoteList = deliveryNoteRepository.findAll();
        assertThat(deliveryNoteList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkDeliveryAdressIsRequired() throws Exception {
        int databaseSizeBeforeTest = deliveryNoteRepository.findAll().size();
        // set the field null
        deliveryNote.setDeliveryAdress(null);

        // Create the DeliveryNote, which fails.

        restDeliveryNoteMockMvc.perform(post("/api/delivery-notes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNote)))
            .andExpect(status().isBadRequest());

        List<DeliveryNote> deliveryNoteList = deliveryNoteRepository.findAll();
        assertThat(deliveryNoteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupplierFirmIsRequired() throws Exception {
        int databaseSizeBeforeTest = deliveryNoteRepository.findAll().size();
        // set the field null
        deliveryNote.setSupplierFirm(null);

        // Create the DeliveryNote, which fails.

        restDeliveryNoteMockMvc.perform(post("/api/delivery-notes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNote)))
            .andExpect(status().isBadRequest());

        List<DeliveryNote> deliveryNoteList = deliveryNoteRepository.findAll();
        assertThat(deliveryNoteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkSupplierAdressIsRequired() throws Exception {
        int databaseSizeBeforeTest = deliveryNoteRepository.findAll().size();
        // set the field null
        deliveryNote.setSupplierAdress(null);

        // Create the DeliveryNote, which fails.

        restDeliveryNoteMockMvc.perform(post("/api/delivery-notes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNote)))
            .andExpect(status().isBadRequest());

        List<DeliveryNote> deliveryNoteList = deliveryNoteRepository.findAll();
        assertThat(deliveryNoteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkFinishedIsRequired() throws Exception {
        int databaseSizeBeforeTest = deliveryNoteRepository.findAll().size();
        // set the field null
        deliveryNote.setFinished(null);

        // Create the DeliveryNote, which fails.

        restDeliveryNoteMockMvc.perform(post("/api/delivery-notes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNote)))
            .andExpect(status().isBadRequest());

        List<DeliveryNote> deliveryNoteList = deliveryNoteRepository.findAll();
        assertThat(deliveryNoteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkPrincipalIsRequired() throws Exception {
        int databaseSizeBeforeTest = deliveryNoteRepository.findAll().size();
        // set the field null
        deliveryNote.setPrincipal(null);

        // Create the DeliveryNote, which fails.

        restDeliveryNoteMockMvc.perform(post("/api/delivery-notes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNote)))
            .andExpect(status().isBadRequest());

        List<DeliveryNote> deliveryNoteList = deliveryNoteRepository.findAll();
        assertThat(deliveryNoteList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDeliveryNotes() throws Exception {
        // Initialize the database
        deliveryNoteRepository.saveAndFlush(deliveryNote);

        // Get all the deliveryNoteList
        restDeliveryNoteMockMvc.perform(get("/api/delivery-notes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(deliveryNote.getId().intValue())))
            .andExpect(jsonPath("$.[*].noteNumber").value(hasItem(DEFAULT_NOTE_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].deliveryAdress").value(hasItem(DEFAULT_DELIVERY_ADRESS.toString())))
            .andExpect(jsonPath("$.[*].supplierFirm").value(hasItem(DEFAULT_SUPPLIER_FIRM.toString())))
            .andExpect(jsonPath("$.[*].supplierAdress").value(hasItem(DEFAULT_SUPPLIER_ADRESS.toString())))
            .andExpect(jsonPath("$.[*].issueDate").value(hasItem(DEFAULT_ISSUE_DATE.toString())))
            .andExpect(jsonPath("$.[*].finished").value(hasItem(DEFAULT_FINISHED.booleanValue())))
            .andExpect(jsonPath("$.[*].principal").value(hasItem(DEFAULT_PRINCIPAL.toString())));
    }
    
    @Test
    @Transactional
    public void getDeliveryNote() throws Exception {
        // Initialize the database
        deliveryNoteRepository.saveAndFlush(deliveryNote);

        // Get the deliveryNote
        restDeliveryNoteMockMvc.perform(get("/api/delivery-notes/{id}", deliveryNote.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(deliveryNote.getId().intValue()))
            .andExpect(jsonPath("$.noteNumber").value(DEFAULT_NOTE_NUMBER.toString()))
            .andExpect(jsonPath("$.deliveryAdress").value(DEFAULT_DELIVERY_ADRESS.toString()))
            .andExpect(jsonPath("$.supplierFirm").value(DEFAULT_SUPPLIER_FIRM.toString()))
            .andExpect(jsonPath("$.supplierAdress").value(DEFAULT_SUPPLIER_ADRESS.toString()))
            .andExpect(jsonPath("$.issueDate").value(DEFAULT_ISSUE_DATE.toString()))
            .andExpect(jsonPath("$.finished").value(DEFAULT_FINISHED.booleanValue()))
            .andExpect(jsonPath("$.principal").value(DEFAULT_PRINCIPAL.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingDeliveryNote() throws Exception {
        // Get the deliveryNote
        restDeliveryNoteMockMvc.perform(get("/api/delivery-notes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDeliveryNote() throws Exception {
        // Initialize the database
        deliveryNoteService.save(deliveryNote);

        int databaseSizeBeforeUpdate = deliveryNoteRepository.findAll().size();

        // Update the deliveryNote
        DeliveryNote updatedDeliveryNote = deliveryNoteRepository.findById(deliveryNote.getId()).get();
        // Disconnect from session so that the updates on updatedDeliveryNote are not directly saved in db
        em.detach(updatedDeliveryNote);
        updatedDeliveryNote
            .noteNumber(UPDATED_NOTE_NUMBER)
            .deliveryAdress(UPDATED_DELIVERY_ADRESS)
            .supplierFirm(UPDATED_SUPPLIER_FIRM)
            .supplierAdress(UPDATED_SUPPLIER_ADRESS)
            .issueDate(UPDATED_ISSUE_DATE)
            .finished(UPDATED_FINISHED)
            .principal(UPDATED_PRINCIPAL);

        restDeliveryNoteMockMvc.perform(put("/api/delivery-notes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDeliveryNote)))
            .andExpect(status().isOk());

        // Validate the DeliveryNote in the database
        List<DeliveryNote> deliveryNoteList = deliveryNoteRepository.findAll();
        assertThat(deliveryNoteList).hasSize(databaseSizeBeforeUpdate);
        DeliveryNote testDeliveryNote = deliveryNoteList.get(deliveryNoteList.size() - 1);
        assertThat(testDeliveryNote.getNoteNumber()).isEqualTo(UPDATED_NOTE_NUMBER);
        assertThat(testDeliveryNote.getDeliveryAdress()).isEqualTo(UPDATED_DELIVERY_ADRESS);
        assertThat(testDeliveryNote.getSupplierFirm()).isEqualTo(UPDATED_SUPPLIER_FIRM);
        assertThat(testDeliveryNote.getSupplierAdress()).isEqualTo(UPDATED_SUPPLIER_ADRESS);
        assertThat(testDeliveryNote.getIssueDate()).isEqualTo(UPDATED_ISSUE_DATE);
        assertThat(testDeliveryNote.isFinished()).isEqualTo(UPDATED_FINISHED);
        assertThat(testDeliveryNote.getPrincipal()).isEqualTo(UPDATED_PRINCIPAL);
    }

    @Test
    @Transactional
    public void updateNonExistingDeliveryNote() throws Exception {
        int databaseSizeBeforeUpdate = deliveryNoteRepository.findAll().size();

        // Create the DeliveryNote

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDeliveryNoteMockMvc.perform(put("/api/delivery-notes")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(deliveryNote)))
            .andExpect(status().isBadRequest());

        // Validate the DeliveryNote in the database
        List<DeliveryNote> deliveryNoteList = deliveryNoteRepository.findAll();
        assertThat(deliveryNoteList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDeliveryNote() throws Exception {
        // Initialize the database
        deliveryNoteService.save(deliveryNote);

        int databaseSizeBeforeDelete = deliveryNoteRepository.findAll().size();

        // Get the deliveryNote
        restDeliveryNoteMockMvc.perform(delete("/api/delivery-notes/{id}", deliveryNote.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<DeliveryNote> deliveryNoteList = deliveryNoteRepository.findAll();
        assertThat(deliveryNoteList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(DeliveryNote.class);
        DeliveryNote deliveryNote1 = new DeliveryNote();
        deliveryNote1.setId(1L);
        DeliveryNote deliveryNote2 = new DeliveryNote();
        deliveryNote2.setId(deliveryNote1.getId());
        assertThat(deliveryNote1).isEqualTo(deliveryNote2);
        deliveryNote2.setId(2L);
        assertThat(deliveryNote1).isNotEqualTo(deliveryNote2);
        deliveryNote1.setId(null);
        assertThat(deliveryNote1).isNotEqualTo(deliveryNote2);
    }
}
