package de.johenneken.soa.deliverynote.repository;

import de.johenneken.soa.deliverynote.domain.DeliveryNoteEntry;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;


/**
 * Spring Data  repository for the DeliveryNoteEntry entity.
 */
@SuppressWarnings("unused")
@Repository
public interface DeliveryNoteEntryRepository extends JpaRepository<DeliveryNoteEntry, Long> {

}
