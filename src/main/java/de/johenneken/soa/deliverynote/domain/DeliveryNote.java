package de.johenneken.soa.deliverynote.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.ReadOnlyProperty;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;
import java.util.Objects;

/**
 * A DeliveryNote.
 */
@Entity
@Table(name = "delivery_note")
public class DeliveryNote implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "note_number")
    private String noteNumber;

    @NotNull
    @Column(name = "delivery_adress", nullable = false)
    private String deliveryAdress;

    @NotNull
    @Column(name = "supplier_firm", nullable = false)
    private String supplierFirm;

    @NotNull
    @Column(name = "supplier_adress", nullable = false)
    private String supplierAdress;

    @Column(name = "issue_date")
    private LocalDate issueDate;

    @NotNull
    @Column(name = "finished", nullable = false)
    private Boolean finished;

    @Column(name = "principal", nullable = false)
    @ReadOnlyProperty
    private String principal;

    @OneToMany(mappedBy = "deliveryNote", fetch = FetchType.EAGER)
    @JsonIgnoreProperties("deliveryNote")
    private Set<DeliveryNoteEntry> entries = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNoteNumber() {
        return noteNumber;
    }

    public DeliveryNote noteNumber(String noteNumber) {
        this.noteNumber = noteNumber;
        return this;
    }

    public void setNoteNumber(String noteNumber) {
        this.noteNumber = noteNumber;
    }

    public String getDeliveryAdress() {
        return deliveryAdress;
    }

    public DeliveryNote deliveryAdress(String deliveryAdress) {
        this.deliveryAdress = deliveryAdress;
        return this;
    }

    public void setDeliveryAdress(String deliveryAdress) {
        this.deliveryAdress = deliveryAdress;
    }

    public String getSupplierFirm() {
        return supplierFirm;
    }

    public DeliveryNote supplierFirm(String supplierFirm) {
        this.supplierFirm = supplierFirm;
        return this;
    }

    public void setSupplierFirm(String supplierFirm) {
        this.supplierFirm = supplierFirm;
    }

    public String getSupplierAdress() {
        return supplierAdress;
    }

    public DeliveryNote supplierAdress(String supplierAdress) {
        this.supplierAdress = supplierAdress;
        return this;
    }

    public void setSupplierAdress(String supplierAdress) {
        this.supplierAdress = supplierAdress;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public DeliveryNote issueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
        return this;
    }

    public void setIssueDate(LocalDate issueDate) {
        this.issueDate = issueDate;
    }

    public Boolean isFinished() {
        return finished;
    }

    public DeliveryNote finished(Boolean finished) {
        this.finished = finished;
        return this;
    }

    public void setFinished(Boolean finished) {
        this.finished = finished;
    }

    public String getPrincipal() {
        return principal;
    }

    public DeliveryNote principal(String principal) {
        this.principal = principal;
        return this;
    }

    public void setPrincipal(String principal) {
        this.principal = principal;
    }

    public Set<DeliveryNoteEntry> getEntries() {
        return entries;
    }

    public DeliveryNote entries(Set<DeliveryNoteEntry> deliveryNoteEntries) {
        this.entries = deliveryNoteEntries;
        return this;
    }

    public DeliveryNote addEntries(DeliveryNoteEntry deliveryNoteEntry) {
        this.entries.add(deliveryNoteEntry);
        deliveryNoteEntry.setDeliveryNote(this);
        return this;
    }

    public DeliveryNote removeEntries(DeliveryNoteEntry deliveryNoteEntry) {
        this.entries.remove(deliveryNoteEntry);
        deliveryNoteEntry.setDeliveryNote(null);
        return this;
    }

    public void setEntries(Set<DeliveryNoteEntry> deliveryNoteEntries) {
        this.entries = deliveryNoteEntries;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DeliveryNote deliveryNote = (DeliveryNote) o;
        if (deliveryNote.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), deliveryNote.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "DeliveryNote{" +
            "id=" + getId() +
            ", noteNumber='" + getNoteNumber() + "'" +
            ", deliveryAdress='" + getDeliveryAdress() + "'" +
            ", supplierFirm='" + getSupplierFirm() + "'" +
            ", supplierAdress='" + getSupplierAdress() + "'" +
            ", issueDate='" + getIssueDate() + "'" +
            ", finished='" + isFinished() + "'" +
            ", principal='" + getPrincipal() + "'" +
            "}";
    }
}
