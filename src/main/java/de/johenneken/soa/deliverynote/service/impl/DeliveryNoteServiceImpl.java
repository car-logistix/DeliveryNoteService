package de.johenneken.soa.deliverynote.service.impl;

import de.johenneken.soa.deliverynote.repository.DeliveryNoteEntryRepository;
import de.johenneken.soa.deliverynote.security.SecurityUtils;
import de.johenneken.soa.deliverynote.service.DeliveryNoteService;
import de.johenneken.soa.deliverynote.domain.DeliveryNote;
import de.johenneken.soa.deliverynote.repository.DeliveryNoteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing DeliveryNote.
 */
@Service
@Transactional
public class DeliveryNoteServiceImpl implements DeliveryNoteService {

    private final Logger log = LoggerFactory.getLogger(DeliveryNoteServiceImpl.class);

    private final DeliveryNoteRepository deliveryNoteRepository;
    private final DeliveryNoteEntryRepository deliveryNoteEntryRepository;

    public DeliveryNoteServiceImpl(DeliveryNoteRepository deliveryNoteRepository, DeliveryNoteEntryRepository deliveryNoteEntryRepository) {
        this.deliveryNoteRepository = deliveryNoteRepository;
        this.deliveryNoteEntryRepository = deliveryNoteEntryRepository;
    }

    /**
     * Save a deliveryNote.
     *
     * @param deliveryNote the entity to save
     * @return the persisted entity
     */
    @Override
    public DeliveryNote save(DeliveryNote deliveryNote) {
        log.debug("Request to save DeliveryNote : {}", deliveryNote);
        if(deliveryNote.getId()==null) {
            deliveryNote.setPrincipal(SecurityUtils.getCurrentUserLogin().get());
        }
        deliveryNote.getEntries().forEach(deliveryNoteEntry -> deliveryNoteEntry.setDeliveryNote(deliveryNote));
        deliveryNoteEntryRepository.saveAll(deliveryNote.getEntries());
        return deliveryNoteRepository.save(deliveryNote);
    }

    /**
     * Get all the deliveryNotes.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<DeliveryNote> findAll() {
        log.debug("Request to get all DeliveryNotes");
        return deliveryNoteRepository.findAll();
    }


    /**
     * Get one deliveryNote by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<DeliveryNote> findOne(Long id) {
        log.debug("Request to get DeliveryNote : {}", id);
        return deliveryNoteRepository.findById(id);
    }

    /**
     * Delete the deliveryNote by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete DeliveryNote : {}", id);
        deliveryNoteRepository.deleteById(id);
    }
}
