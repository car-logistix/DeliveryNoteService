package de.johenneken.soa.deliverynote.service;

import de.johenneken.soa.deliverynote.domain.DeliveryNote;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing DeliveryNote.
 */
public interface DeliveryNoteService {

    /**
     * Save a deliveryNote.
     *
     * @param deliveryNote the entity to save
     * @return the persisted entity
     */
    DeliveryNote save(DeliveryNote deliveryNote);

    /**
     * Get all the deliveryNotes.
     *
     * @return the list of entities
     */
    List<DeliveryNote> findAll();


    /**
     * Get the "id" deliveryNote.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<DeliveryNote> findOne(Long id);

    /**
     * Delete the "id" deliveryNote.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
