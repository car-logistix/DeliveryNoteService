package de.johenneken.soa.deliverynote.web.rest;

import com.codahale.metrics.annotation.Timed;
import de.johenneken.soa.deliverynote.domain.DeliveryNoteEntry;
import de.johenneken.soa.deliverynote.repository.DeliveryNoteEntryRepository;
import de.johenneken.soa.deliverynote.web.rest.errors.BadRequestAlertException;
import de.johenneken.soa.deliverynote.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DeliveryNoteEntry.
 */
@RestController
@RequestMapping("/api")
public class DeliveryNoteEntryResource {

    private final Logger log = LoggerFactory.getLogger(DeliveryNoteEntryResource.class);

    private static final String ENTITY_NAME = "deliveryNoteServiceDeliveryNoteEntry";

    private final DeliveryNoteEntryRepository deliveryNoteEntryRepository;

    public DeliveryNoteEntryResource(DeliveryNoteEntryRepository deliveryNoteEntryRepository) {
        this.deliveryNoteEntryRepository = deliveryNoteEntryRepository;
    }

    /**
     * POST  /delivery-note-entries : Create a new deliveryNoteEntry.
     *
     * @param deliveryNoteEntry the deliveryNoteEntry to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deliveryNoteEntry, or with status 400 (Bad Request) if the deliveryNoteEntry has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/delivery-note-entries")
    @Timed
    public ResponseEntity<DeliveryNoteEntry> createDeliveryNoteEntry(@RequestBody DeliveryNoteEntry deliveryNoteEntry) throws URISyntaxException {
        log.debug("REST request to save DeliveryNoteEntry : {}", deliveryNoteEntry);
        if (deliveryNoteEntry.getId() != null) {
            throw new BadRequestAlertException("A new deliveryNoteEntry cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DeliveryNoteEntry result = deliveryNoteEntryRepository.save(deliveryNoteEntry);
        return ResponseEntity.created(new URI("/api/delivery-note-entries/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /delivery-note-entries : Updates an existing deliveryNoteEntry.
     *
     * @param deliveryNoteEntry the deliveryNoteEntry to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deliveryNoteEntry,
     * or with status 400 (Bad Request) if the deliveryNoteEntry is not valid,
     * or with status 500 (Internal Server Error) if the deliveryNoteEntry couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/delivery-note-entries")
    @Timed
    public ResponseEntity<DeliveryNoteEntry> updateDeliveryNoteEntry(@RequestBody DeliveryNoteEntry deliveryNoteEntry) throws URISyntaxException {
        log.debug("REST request to update DeliveryNoteEntry : {}", deliveryNoteEntry);
        if (deliveryNoteEntry.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DeliveryNoteEntry result = deliveryNoteEntryRepository.save(deliveryNoteEntry);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, deliveryNoteEntry.getId().toString()))
            .body(result);
    }

    /**
     * GET  /delivery-note-entries : get all the deliveryNoteEntries.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of deliveryNoteEntries in body
     */
    @GetMapping("/delivery-note-entries")
    @Timed
    public List<DeliveryNoteEntry> getAllDeliveryNoteEntries() {
        log.debug("REST request to get all DeliveryNoteEntries");
        return deliveryNoteEntryRepository.findAll();
    }

    /**
     * GET  /delivery-note-entries/:id : get the "id" deliveryNoteEntry.
     *
     * @param id the id of the deliveryNoteEntry to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deliveryNoteEntry, or with status 404 (Not Found)
     */
    @GetMapping("/delivery-note-entries/{id}")
    @Timed
    public ResponseEntity<DeliveryNoteEntry> getDeliveryNoteEntry(@PathVariable Long id) {
        log.debug("REST request to get DeliveryNoteEntry : {}", id);
        Optional<DeliveryNoteEntry> deliveryNoteEntry = deliveryNoteEntryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(deliveryNoteEntry);
    }

    /**
     * DELETE  /delivery-note-entries/:id : delete the "id" deliveryNoteEntry.
     *
     * @param id the id of the deliveryNoteEntry to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/delivery-note-entries/{id}")
    @Timed
    public ResponseEntity<Void> deleteDeliveryNoteEntry(@PathVariable Long id) {
        log.debug("REST request to delete DeliveryNoteEntry : {}", id);

        deliveryNoteEntryRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
