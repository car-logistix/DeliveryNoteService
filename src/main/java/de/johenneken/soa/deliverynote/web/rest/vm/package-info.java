/**
 * View Models used by Spring MVC REST controllers.
 */
package de.johenneken.soa.deliverynote.web.rest.vm;
