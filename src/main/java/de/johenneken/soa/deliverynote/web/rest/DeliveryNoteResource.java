package de.johenneken.soa.deliverynote.web.rest;

import com.codahale.metrics.annotation.Timed;
import de.johenneken.soa.deliverynote.domain.DeliveryNote;
import de.johenneken.soa.deliverynote.service.DeliveryNoteService;
import de.johenneken.soa.deliverynote.web.rest.errors.BadRequestAlertException;
import de.johenneken.soa.deliverynote.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing DeliveryNote.
 */
@RestController
@RequestMapping("/api")
public class DeliveryNoteResource {

    private final Logger log = LoggerFactory.getLogger(DeliveryNoteResource.class);

    private static final String ENTITY_NAME = "deliveryNoteServiceDeliveryNote";

    private final DeliveryNoteService deliveryNoteService;

    public DeliveryNoteResource(DeliveryNoteService deliveryNoteService) {
        this.deliveryNoteService = deliveryNoteService;
    }

    /**
     * POST  /delivery-notes : Create a new deliveryNote.
     *
     * @param deliveryNote the deliveryNote to create
     * @return the ResponseEntity with status 201 (Created) and with body the new deliveryNote, or with status 400 (Bad Request) if the deliveryNote has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/delivery-notes")
    @Timed
    public ResponseEntity<DeliveryNote> createDeliveryNote(@Valid @RequestBody DeliveryNote deliveryNote) throws URISyntaxException {
        log.debug("REST request to save DeliveryNote : {}", deliveryNote);
        if (deliveryNote.getId() != null) {
            throw new BadRequestAlertException("A new deliveryNote cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DeliveryNote result = deliveryNoteService.save(deliveryNote);
        return ResponseEntity.created(new URI("/api/delivery-notes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /delivery-notes : Updates an existing deliveryNote.
     *
     * @param deliveryNote the deliveryNote to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated deliveryNote,
     * or with status 400 (Bad Request) if the deliveryNote is not valid,
     * or with status 500 (Internal Server Error) if the deliveryNote couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/delivery-notes")
    @Timed
    public ResponseEntity<DeliveryNote> updateDeliveryNote(@Valid @RequestBody DeliveryNote deliveryNote) throws URISyntaxException {
        log.debug("REST request to update DeliveryNote : {}", deliveryNote);
        if (deliveryNote.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DeliveryNote result = deliveryNoteService.save(deliveryNote);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, deliveryNote.getId().toString()))
            .body(result);
    }

    /**
     * GET  /delivery-notes : get all the deliveryNotes.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of deliveryNotes in body
     */
    @GetMapping("/delivery-notes")
    @Timed
    public List<DeliveryNote> getAllDeliveryNotes() {
        log.debug("REST request to get all DeliveryNotes");
        return deliveryNoteService.findAll();
    }

    /**
     * GET  /delivery-notes/:id : get the "id" deliveryNote.
     *
     * @param id the id of the deliveryNote to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the deliveryNote, or with status 404 (Not Found)
     */
    @GetMapping("/delivery-notes/{id}")
    @Timed
    public ResponseEntity<DeliveryNote> getDeliveryNote(@PathVariable Long id) {
        log.debug("REST request to get DeliveryNote : {}", id);
        Optional<DeliveryNote> deliveryNote = deliveryNoteService.findOne(id);
        return ResponseUtil.wrapOrNotFound(deliveryNote);
    }

    /**
     * DELETE  /delivery-notes/:id : delete the "id" deliveryNote.
     *
     * @param id the id of the deliveryNote to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/delivery-notes/{id}")
    @Timed
    public ResponseEntity<Void> deleteDeliveryNote(@PathVariable Long id) {
        log.debug("REST request to delete DeliveryNote : {}", id);
        deliveryNoteService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
